const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const contactsRoute = require('./routes/contactsRoute');
const contactModel = require('./models/contactModel');

const app = express();
const port = process.env.PORT || 3000;
const auth = require('./middlewares/auth');

// database connection
mongoose.connect('mongodb://localhost/vapulus');

// middlewares
app.use(bodyParser.json());
app.use(auth);

// routes
app.use('/contacts', contactsRoute);

app.listen(port, () => console.log(`Server up and running at http://localhost:${port}`));