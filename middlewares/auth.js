const users = require('../config');

module.exports = function (req, resp, next) {
    users.forEach(function (user) {
        if (user.authorization === req.body.authorization 
            && user.deviceToken === req.body.deviceToken
            && user.fingerPrint === req.body.fingerPrint
        ) {
            req.isAuthenticated = true;
            req.userId = user.userId;
            req.userName = user.name;
            return;
        }
    });
    if (req.isAuthenticated) {
        next();
    } else {
        resp.status(401).json({
            message: 'user is not authorized'
        });
    }
};