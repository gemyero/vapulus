const mongoose = require('mongoose');
const validator = require('validator');
const uniqueValidator = require('mongoose-unique-validator');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const contactSchema = Schema({
    firstname: {
        type: String,
        required: [true, 'firstname is required'],
        validate: {
            validator: (name) => validator.isAlpha(name),
            message: '{VALUE} is not a valid name!'
        }
    },
    lastname: {
        type: String,
        required: [true, 'lastname is required'],
        validate: {
            validator: (name) => validator.isAlpha(name),
            message: '{VALUE} is not a valid name!'
        }
    },
    email: {
        type: String,
        required: [true, 'email is required'],
        index: true,
        validate: {
            validator: (email) => validator.isEmail(email),
            message: '{VALUE} is not a valid email!'
        }
    },
    mobile: {
        type: String,
        required: [true, 'mobile number is required'],
        validate: {
            validator: (phone) => /^01[0-2]{1}[0-9]{8}$/.test(phone),
            message: '{VALUE} is not a valid phone number!'
        }
    },
    userId: {
        type: Number,
        index: true
    },
    createAt: {
        type: Date,
        default: Date.now
    }
});

// add unique to both userId and email 
// each user can create contacts with unique emails
contactSchema.index({email: 1, userId: 1}, {unique: true});

// add plugins
contactSchema.plugin(uniqueValidator);
contactSchema.plugin(mongoosePaginate);


contactSchema.statics.addContact = (firstname, lastname, email, mobile, userId, resolve, reject) => {
    let contact = new Contact({
        firstname,
        lastname,
        email,
        mobile,
        userId
    });

    contact.save().then(resolve).catch(reject);
};

contactSchema.statics.getListOfContacts = function(id, pageNumber, resolve, reject) {
    this.paginate({userId: id}, {page: pageNumber, limit: 2})
        .then(resolve)
        .catch(reject);
}

contactSchema.statics.getRecentContacts = function (id, resolve, reject) {
    this.find({userId: id})
        .sort({createAt: -1})
        .limit(5)
        .exec()
        .then(resolve)
        .catch(reject);
}

const Contact = mongoose.model('contacts', contactSchema);

module.exports = Contact;