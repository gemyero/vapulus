const express = require('express');
const route = express.Router();
const contactModel = require('../models/contactModel');

route.post('/addContact', (req, resp) => {
    contactModel.addContact(req.body.firstname,
        req.body.lastname,
        req.body.email,
        req.body.mobile,
        req.userId,
        (doc) => {
            resp.json({
                statusCode: 200,
                message: 'Contact add successfully!',
                data: {
                    email: doc.email,
                    contactId: doc._id,
                    userId: doc.userId,
                    firstname: doc.firstname,
                    lastname: doc.lastname,
                    mobileNumber: doc.mobile
                }
            });
        },
        (err) => {
            let errors = [];
            for (let key in err.errors) {
                errors.push(err.errors[key].message);
            }
            resp.json(errors);
        }
    );
});

route.post('/getList', (req, resp) => {
    contactModel.getListOfContacts(
        req.userId,
        req.body.pageNumber,
        (docs) => {
            const contacts = docs.docs;
            resp.json({
                statusCode: 200,
                message: "List all user contacts",
                data: contacts.map(function (contact) {
                    let newContact = {};
                    newContact.createAt = contact.createAt;
                    newContact.firstname = contact.firstname;
                    newContact.lastname = contact.lastname;
                    newContact.userId = contact.userId;
                    newContact.contactId = contact._id;
                    newContact.mobileNumber = contact.mobile;
                    newContact.email = contact.email;
                    return newContact;
                })
            });
        }
    );
});

route.post('/getRecentList', (req, resp) => {
    contactModel.getRecentContacts(
        req.userId,
        (contacts) => {
            resp.json({
                statusCode: 200,
                message: "List recent contacts",
                data: contacts.map(function (contact) {
                    let newContact = {};
                    newContact.created_ts = +contact.createAt;
                    newContact.userId = contact.userId;
                    newContact.email = contact.email;
                    newContact.firstname = contact.firstname;
                    newContact.lastname = contact.lastname;
                    newContact.mobileNumber = contact.mobile;
                    return newContact;
                })
            });
        }
    );
});

module.exports = route;